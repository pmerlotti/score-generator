﻿using System;
using ScoreRecorder.Controls.Scoreboards;
using ScoreRecorder.Core;

namespace ScoringSample
{

    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Match Recorder 1.0 - by Paulo Merlotti");
            Console.WriteLine("---");
            Console.WriteLine("<SPACEBAR> Start, pause and restart the match");
            Console.WriteLine("<ENTER> Ends the match");
            Console.WriteLine("<1> Scores for home team");
            Console.WriteLine("<3> Scores for guest team");

            var teams = new Teams();

            //get team names
            Console.Write("Enter name for HOME team: ");
            teams.HomeTeamShort = Console.ReadLine();
            Console.Write("Enter name for GUEST team: ");
            teams.GuestTeamShort = Console.ReadLine();

            var recorder = new MatchRecorder(new MatchStopWatch(), teams);

            bool go = true;
            while (go)
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);

                switch (keyInfo.Key)
                {
                    case ConsoleKey.Spacebar:
                        recorder.Start();
                        Console.WriteLine("Match Started");
                        break;
                    case ConsoleKey.Enter:
                        recorder.EndMatch();
                        go = false;
                        break;
                    case ConsoleKey.NumPad1:
                        recorder.IncreaseScore(TeamType.Home);
                        Console.WriteLine(recorder.State.ToString());
                        break;
                    case ConsoleKey.NumPad3:
                        recorder.IncreaseScore(TeamType.Guest);
                        Console.WriteLine(recorder.State.ToString());
                        break;
                    default:
                        break;
                }
            }

            //print final match state
            Console.WriteLine("FINAL MATCH STATE");
            Console.WriteLine(recorder.State.ToString());

            Console.Write("Enter video file name: ");
            var fileName = Console.ReadLine();

            //write to video
            Console.WriteLine("Creating video file...");
            IScoreboard scoreboard = new SimpleScoreboard();
            var writter = new MatchVideoWritter(recorder.EventLogger, scoreboard, teams);
            writter.CreateVideo(fileName);

            Console.WriteLine("Press any key to end...");
            Console.ReadKey();
        }
    }
}
