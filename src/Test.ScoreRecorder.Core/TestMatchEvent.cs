﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScoreRecorder.Core;

namespace Test.ScoreRecorder.Core
{
    [TestClass]
    public class TestMatchEvent
    {
        [TestMethod]
        public void TestCreatingScoringEvent()
        {
            var se = new ScoringEvent()
            {
                EventElapsed = TimeSpan.FromMilliseconds(100),
                TeamType = TeamType.Home,
                IncreaseOrDecrease = ScoringDirectionType.Increase
            };

            Assert.AreEqual(100, se.EventElapsed.Milliseconds);
            Assert.AreEqual(TeamType.Home, se.TeamType);
            Assert.AreEqual(ScoringDirectionType.Increase, se.IncreaseOrDecrease);
        }
    }
}
