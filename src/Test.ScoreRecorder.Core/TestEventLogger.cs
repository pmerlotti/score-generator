﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScoreRecorder.Core;

namespace Test.ScoreRecorder.Core
{
    [TestClass]
    public class TestEventLogger
    {
        [TestMethod]
        public void TestAddingEventLog()
        {
            var el = new EventLogger();
            var sw = new FakeStopWatch();

            el.LogEvent(new ScoringEvent()
            {
                EventElapsed = sw.Elapsed,
                IncreaseOrDecrease = ScoringDirectionType.Increase,
                TeamType = TeamType.Guest
            });

            Assert.AreEqual(1, el.Log.Count);
        }

    }
}
