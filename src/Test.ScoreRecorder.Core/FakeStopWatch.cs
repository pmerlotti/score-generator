﻿using ScoreRecorder.Core;
using System;

namespace Test.ScoreRecorder.Core
{
    internal class FakeStopWatch : IStopWatch
    {
        private TimeSpan timeSpan = new TimeSpan();

        public bool IsRunning { get; private set; }

        public TimeSpan Elapsed { get { return timeSpan; } }

        public long ElapsedMilliseconds { get { return timeSpan.Milliseconds; } }

        public long ElapsedTicks { get { return timeSpan.Ticks; } }

        public void Reset()
        {
            timeSpan = TimeSpan.FromMilliseconds(0);
        }

        public void Restart()
        {
            IsRunning = true;
        }

        public void Start()
        {
            IsRunning = true;
            timeSpan = TimeSpan.FromMilliseconds(0);
        }

        public void Stop()
        {
            IsRunning = false;
        }

        public void SetMilliseconds(double milliseconds)
        {
            timeSpan = TimeSpan.FromMilliseconds(milliseconds);
        }

        public string ToLongElapsedTimeString()
        {
            return Elapsed.ToString(@"hh\:mm\:ss\.fff");
        }
    }
}
