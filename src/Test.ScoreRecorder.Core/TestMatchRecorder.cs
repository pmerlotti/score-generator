﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScoreRecorder.Core;

namespace Test.ScoreRecorder.Core
{
    [TestClass]
    public class TestMatchRecorder
    {
        [TestMethod]
        public void TestStartStop()
        {
            var sw = new FakeStopWatch();

            var mr = new MatchRecorder(sw, Utils.Teams);

            mr.Start();
            Assert.IsTrue(sw.IsRunning);

            sw.SetMilliseconds(1000);
            mr.EndMatch();
            Assert.IsFalse(sw.IsRunning);
        }

        [TestMethod]
        public void TestScoring()
        {
            var sw = new FakeStopWatch();
            var mr = new MatchRecorder(sw, Utils.Teams);

            mr.Start();


            //1-0 for home team at 1000ms
            sw.SetMilliseconds(1000);
            mr.IncreaseScore(TeamType.Home);
            Assert.AreEqual(1000, mr.EventLogger.Log.Keys[1].TotalMilliseconds);
            Assert.AreEqual(1, mr.State.HomeTeamScore);
            Assert.AreEqual(0, mr.State.GuestTeamScore);

            //1-1 for home team at 2000ms
            sw.SetMilliseconds(2000);
            mr.IncreaseScore(TeamType.Guest);
            Assert.AreEqual(2000, mr.EventLogger.Log.Keys[2].TotalMilliseconds);
            Assert.AreEqual(1, mr.State.HomeTeamScore);
            Assert.AreEqual(1, mr.State.GuestTeamScore);

            //0-1 for home team at 3000ms
            sw.SetMilliseconds(3000);
            mr.DecreaseScore(TeamType.Home);
            Assert.AreEqual(3000, mr.EventLogger.Log.Keys[3].TotalMilliseconds);
            Assert.AreEqual(0, mr.State.HomeTeamScore);
            Assert.AreEqual(1, mr.State.GuestTeamScore);

            //0-0 for home team at 4000ms
            sw.SetMilliseconds(4000);
            mr.DecreaseScore(TeamType.Guest);
            Assert.AreEqual(4000, mr.EventLogger.Log.Keys[4].TotalMilliseconds);
            Assert.AreEqual(0, mr.State.HomeTeamScore);
            Assert.AreEqual(0, mr.State.GuestTeamScore);
        }

        [TestMethod]
        public void TestEndMatch()
        {
            var sw = new FakeStopWatch();
            var mr = new MatchRecorder(sw, Utils.Teams);

            mr.Start();

            sw.SetMilliseconds(2000);
            mr.EndMatch();

            Assert.IsTrue(mr.HasEnded);
        }
    }
}
