﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScoreRecorder.Core;
using System;

namespace Test.ScoreRecorder.Core
{
    [TestClass]
    public class TestMatchState
    {
        [TestMethod]
        public void TestSetElapsed()
        {
            var st = new MatchState(TimeSpan.FromMilliseconds(1000));

            Assert.AreEqual(1000, st.Elapsed.TotalMilliseconds);
        }
    }
}
