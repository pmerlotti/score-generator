﻿using ScoreRecorder.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.ScoreRecorder.Core
{
    internal static class Utils
    {
        internal static Teams Teams = new Teams()
        {
            HomeTeamName = "Eastlake High School",
            HomeTeamShort = "Eastlake",
            GuestTeamName = "Bishops High School",
            GuestTeamShort = "Bishops"
        };
    }
}
