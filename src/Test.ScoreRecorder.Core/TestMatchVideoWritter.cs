﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScoreRecorder.Core;
using ScoreRecorder.Controls.Scoreboards;

namespace Test.ScoreRecorder.Core
{
    [TestClass]
    public class TestMatchVideoWritter
    {
        [TestMethod]
        [ExpectedException(typeof(MatchNotFinalizedException))]
        public void TestCreateVideoCreatesExceptionIfMatchNotFinalized()
        {
            var eventLogger = new EventLogger();

            eventLogger.LogEvent(new ScoringEvent()
            {
                EventElapsed = TimeSpan.FromMilliseconds(100),
                TeamType = TeamType.Home,
                IncreaseOrDecrease = ScoringDirectionType.Increase
            });

            IScoreboard scoreboard = new SimpleScoreboard();
            var teams = new Teams()
            {
                HomeTeamName = "Eastlake High School",
                HomeTeamShort = "Eastlake",
                GuestTeamName = "Bishops High School",
                GuestTeamShort = "Bishops"
            };

            var videoWritter = new MatchVideoWritter(eventLogger, scoreboard, teams);

            //this should throw an exception because we did not finalized the match
            videoWritter.CreateVideo("video.avi");
        }

        [TestMethod]
        public void TestCreateVideo()
        {
            var eventLogger = new EventLogger();

            eventLogger.LogEvent(new ScoringEvent()
            {
                EventElapsed = TimeSpan.FromMilliseconds(100),
                TeamType = TeamType.Home,
                IncreaseOrDecrease = ScoringDirectionType.Increase
            });

            eventLogger.LogEvent(new EndOfMatchEvent()
            {
                EventElapsed = TimeSpan.FromMilliseconds(1000)
            });

            IScoreboard scoreboard = new SimpleScoreboard();
            var teams = new Teams()
            {
                HomeTeamName = "Eastlake High School",
                HomeTeamShort = "Eastlake",
                GuestTeamName = "Bishops High School",
                GuestTeamShort = "Bishops"
            };

            var videoWritter = new MatchVideoWritter(eventLogger, scoreboard, teams);

            //this should throw an exception because we did not finalized the match
            videoWritter.CreateVideo("video.avi");
        }
    }
}
