﻿using System;
using System.Collections.Generic;

namespace ScoreRecorder.Core
{
    public class EventLogger
    {
        public SortedList<TimeSpan, MatchEvent> Log { get; } = new SortedList<TimeSpan, MatchEvent>();

        public void LogEvent(MatchEvent matchEvent)
        {
            Log.Add(matchEvent.EventElapsed, matchEvent);
        }

        public void Clear()
        {
            Log.Clear();
        }
    }
}