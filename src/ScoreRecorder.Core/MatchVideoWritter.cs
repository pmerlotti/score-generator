﻿using ScoreRecorder.Controls.Scoreboards;
using System;
using System.Diagnostics;
using System.Linq;

namespace ScoreRecorder.Core
{
    public class MatchVideoWritter
    {
        //we need this settings:
        //. video size
        //. location of scoreboard on frame
        //. frame rate
        private static double fps = 30.0;
        private static double fpms = fps / 1000.0;
        private static double t_increment_ms = 1.0 / fpms;

        private readonly EventLogger eventLogger;
        private readonly IScoreboard scoreboard;
        private readonly Teams teams;
        private readonly MatchState matchState = new MatchState(TimeSpan.FromMilliseconds(0));

        public MatchVideoWritter(EventLogger eventLogger, IScoreboard scoreboard, Teams teams)
        {
            this.eventLogger = eventLogger;
            this.scoreboard = scoreboard;
            this.teams = teams;

            matchState.HomeTeamShort = teams.HomeTeamShort;
            matchState.GuestTeamShort = teams.GuestTeamShort;
        }

        public void CreateVideo(string fileName)
        {
            //check if event logger is finalized
            var eom = eventLogger.Log.Values.OfType<EndOfMatchEvent>().FirstOrDefault();
            if (eom == null)
                throw new MatchNotFinalizedException();

            //Use the sorted list of events to get segments of different match states
            //For each segment, render the scoreboard and apply it to all video frames
            //related to that segment
            int w = (int)scoreboard.ControlSize.Width;
            int h = (int)scoreboard.ControlSize.Height;

            var videoWriter = new AVIWriter
            {
                FrameRate = 30
            };
            videoWriter.Open(fileName, w, h);
                

            //get initial match state
            int index = 0;
            double frame_ms = 0.0;
            TimeSpan t = eventLogger.Log.Keys[index];
            MatchEvent ev = eventLogger.Log.Values[index];

            //iterate through segments of time
            while(ev!=eom)
            {
                index++;
                TimeSpan nextT = eventLogger.Log.Keys[index];
                MatchEvent nextEvent = eventLogger.Log.Values[index];

                //create frame with scoreboard for current state
                System.Drawing.Bitmap frameBmp = CreateFrame(ev);

                //calculate time this scoreboard should stay up (until next event)
                double dt_ms = nextT.TotalMilliseconds - t.TotalMilliseconds;
                int frames = (int)(dt_ms * fpms);

                for (int f=0; f<frames; f++)
                {
                    //add to video
                    videoWriter.AddFrame(frameBmp);
                    
                    //increment time code
                    frame_ms += t_increment_ms;
                }

                t = nextT;
                ev = nextEvent;
            }

            //save video
            videoWriter.Close();
        }

        private System.Drawing.Bitmap CreateFrame(MatchEvent matchEvent)
        {
            SetMatchStateFromEvent(matchEvent);
            Debug.WriteLine(matchState.ToString());
            System.Drawing.Bitmap img = scoreboard.Render(matchState);
            return img;
        }

        private void SetMatchStateFromEvent(MatchEvent matchEvent)
        {
            matchState.SetElapsed(matchEvent.EventElapsed);

            if (matchEvent.GetType() == typeof(ScoringEvent))
            {
                var se = matchEvent as ScoringEvent;
                if (se.TeamType == TeamType.Home)
                {
                    if (se.IncreaseOrDecrease == ScoringDirectionType.Increase)
                        matchState.HomeTeamScore++;
                    else
                        matchState.HomeTeamScore--;
                }
                else
                {
                    if (se.IncreaseOrDecrease == ScoringDirectionType.Increase)
                        matchState.GuestTeamScore++;
                    else
                        matchState.GuestTeamScore--;
                }

            }
        }
    }
}
