﻿using ScoreRecorder.Controls.Scoreboards;
using System;

namespace ScoreRecorder.Core
{
    public class MatchState: IMatchState
    {
        private TimeSpan elapsed;

        public MatchState(TimeSpan elapsed)
        {
            this.elapsed = elapsed;
        }

        public TimeSpan Elapsed => elapsed;
        public double ElapsedSeconds => elapsed.TotalSeconds;

        public int SetNumber { get; set; }
        public string HomeTeamShort { get; set; }
        public int HomeTeamGamesWon { get; set; }
        public int HomeTeamScore { get; set; }
        public string GuestTeamShort { get; set; }
        public int GuestTeamGamesWon { get; set; }
        public int GuestTeamScore { get; set; }

        public void SetElapsed(TimeSpan elapsed)
        {
            this.elapsed = elapsed;
        }

        public override string ToString()
        {
            return string.Format("{4:0.0}s\tHome: [{0}] {1} - {3} [{2} :Guest]",
                HomeTeamGamesWon, HomeTeamScore,
                GuestTeamGamesWon, GuestTeamScore,
                ElapsedSeconds);
        }
    }
}
