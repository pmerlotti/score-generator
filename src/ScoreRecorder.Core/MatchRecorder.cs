﻿using System;
using System.Linq;

namespace ScoreRecorder.Core
{

    public class MatchRecorder
    {
        private IStopWatch stopWatch;
        private Teams teams;
        private readonly MatchState matchState;

        public MatchState State 
        { 
            get 
            {
                matchState.SetElapsed(stopWatch.Elapsed);
                return matchState; 
            } 
        }

        public IStopWatch StopWatch => stopWatch;

        public EventLogger EventLogger { get; } = new EventLogger();

        public Teams Teams { get; set; }

        public bool HasEnded
        {
            get
            {
                var eom = EventLogger.Log.Values.OfType<EndOfMatchEvent>().FirstOrDefault();
                return eom != null;
            }
        }

        public MatchRecorder(IStopWatch matchStopWatch, Teams teams)
        {
            stopWatch = matchStopWatch;
            matchState = new MatchState(TimeSpan.FromMilliseconds(0));
            this.teams = teams;
            matchState.SetNumber = 1;
        }

        public void Start()
        {
            stopWatch.Start();
            EventLogger.LogEvent(new StartOfMatchEvent()
            {
                EventElapsed = stopWatch.Elapsed
            });
        }

        public void Reset()
        {
            ResetScore();
            stopWatch.Reset();
            matchState.SetNumber = 0;
            EventLogger.Clear();
        }

        public void ResetScore()
        {
            matchState.HomeTeamScore = 0;
            matchState.GuestTeamScore = 0;
        }

        public void IncreaseScore(TeamType team)
        {
            if (team == TeamType.Home)
                matchState.HomeTeamScore++;
            else
                matchState.GuestTeamScore++;

            EventLogger.LogEvent(new ScoringEvent()
            {
                EventElapsed = stopWatch.Elapsed,
                IncreaseOrDecrease = ScoringDirectionType.Increase,
                TeamType = team
            }) ;
        }

        public void DecreaseScore(TeamType team)
        {
            if (team == TeamType.Home)
                matchState.HomeTeamScore--;
            else
                matchState.GuestTeamScore--;

            EventLogger.LogEvent(new ScoringEvent()
            {
                EventElapsed = stopWatch.Elapsed,
                IncreaseOrDecrease = ScoringDirectionType.Descrease,
                TeamType = team
            });
        }

        public void IncreaseSet()
        {
            matchState.SetNumber++;

            EventLogger.LogEvent(new SetEvent(matchState.SetNumber)
            {
                EventElapsed = stopWatch.Elapsed
            });
        }

        public void DecreaseSet()
        {
            matchState.SetNumber--;

            EventLogger.LogEvent(new SetEvent(matchState.SetNumber)
            {
                EventElapsed = stopWatch.Elapsed
            });
        }

        public void EndMatch()
        {
            stopWatch.Stop();
            EventLogger.LogEvent(new EndOfMatchEvent()
            {
                EventElapsed = stopWatch.Elapsed
            });
        }

        public MatchEvent GetLastEvent()
        {
            return EventLogger.Log.Values.LastOrDefault();
        }

    }
}
