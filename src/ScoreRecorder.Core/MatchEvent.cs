﻿using System;

namespace ScoreRecorder.Core
{
    public abstract class MatchEvent
    {
        public TimeSpan EventElapsed { get; set; }

        public string TimeCode => string.Format(@"{0:hh\:mm\:ss\.fff}", EventElapsed);

        public abstract string Description { get; }
    }

    public class ScoringEvent: MatchEvent
    {
        public TeamType TeamType { get; set; }
        public ScoringDirectionType IncreaseOrDecrease { get; set; }
        public override string Description => $"{TeamType} team score {(IncreaseOrDecrease==ScoringDirectionType.Increase?"up":"down")}";
    }

    public class StartOfMatchEvent : MatchEvent 
    {
        public override string Description => "Match started";
    }

    public class EndOfMatchEvent : MatchEvent 
    {
        public override string Description => "Match finished";
    }

    public class SetEvent : MatchEvent
    {
        public SetEvent(int setNumber)
        {
            SetNumber = setNumber;
        }

        public override string Description => $"Set {SetNumber} started";

        public int SetNumber { get; set; }
    }

    public enum TeamType
    {
        Home,
        Guest
    }

    public enum ScoringDirectionType
    {
        Increase,
        Descrease
    }

}
