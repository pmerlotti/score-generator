﻿namespace ScoreRecorder.Core
{
    public class Teams
    {
        public string HomeTeamName { get; set; }
        public string HomeTeamShort { get; set; }
        public string GuestTeamName { get; set; }
        public string GuestTeamShort { get; set; }
    }
}
