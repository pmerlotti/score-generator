﻿using System;
using System.Diagnostics;

namespace ScoreRecorder.Core
{
    public interface IStopWatch
    {
        bool IsRunning { get; }
        TimeSpan Elapsed { get; }
        long ElapsedMilliseconds { get; }
        long ElapsedTicks { get; }

        string ToLongElapsedTimeString();
        void Reset();
        void Restart();
        void Start();
        void Stop();
    }

    public class MatchStopWatch : Stopwatch, IStopWatch
    {
        public string ToLongElapsedTimeString()
        {
            return Elapsed.ToString(@"hh\:mm\:ss\.fff");
        }
    }
}
