﻿namespace scorec
{
    internal enum AppStates
    {
        Initial,
        Recording,
        Stopped
    }
}