﻿using scorec;
using ScoreRecorder.Core;
using System;
using System.Windows.Threading;

namespace ScoreRecorder
{
    internal class ApplicationState
    {
        //we will try to update the match timer at 24Hz
        private readonly double TimerUpdateRefreshRate = 1000.0 / 24.0;

        private readonly MainWindow main;
        private MatchRecorder matchRecorder;
        private readonly DispatcherTimer dispatchTimer = new DispatcherTimer();

        public ApplicationState(MainWindow mainWindow)
        {
            this.main = mainWindow;
            
            dispatchTimer.Interval = TimeSpan.FromMilliseconds(TimerUpdateRefreshRate);
            dispatchTimer.Tick += DispatchTimer_Tick;
        }

        public AppStates State { get; private set; }

        public void SetInitialState(Core.MatchRecorder matchRecorder)
        {
            State = AppStates.Initial;
            this.matchRecorder = matchRecorder;
            matchRecorder.EventLogger.Clear();
            matchRecorder.Reset();
            dispatchTimer.Stop();

            //disable scoring buttons
            main.ButtonRecord.IsEnabled = true;
            main.ButtonStop.Visibility = System.Windows.Visibility.Hidden;
            main.HomeScoringButtons.IsEnabled = false;
            main.GuestScoringButtons.IsEnabled = false;
            main.SetButtons.IsEnabled = false;

            //set state for side bar menu
            main.ButtonNewMatch.IsEnabled = true;
            main.ButtonOpenMatch.IsEnabled = true;
            main.ButtonSave.IsEnabled = false;
            main.ButtonSaveAs.IsEnabled = false;
            main.ButtonExportVideo.IsEnabled = false;
            main.ButtonExit.IsEnabled = true;

            //clear Teams properties
            main.HomeTeamName = string.Empty;   main.OnPropertyChanged("HomeTeamName");
            main.HomeTeamShort = "Home";        main.OnPropertyChanged("HomeTeamShort");
            main.HomeTeamScore = 0;             main.OnPropertyChanged("HomeTeamScore");
            main.GuestTeamName = string.Empty;  main.OnPropertyChanged("GuestTeamName");
            main.GuestTeamShort = "Guest";      main.OnPropertyChanged("GuestTeamShort");
            main.GuestTeamScore = 0;            main.OnPropertyChanged("GuestTeamScore");

            main.RecordingTimer = matchRecorder.StopWatch.ToLongElapsedTimeString();
            main.OnPropertyChanged("RecordingTimer");
            
           
        }

        internal void StartMatch()
        {   
            State = AppStates.Recording;

            //start match recorder and timers
            matchRecorder.Start();
            dispatchTimer.Start();
            UpdateEventList();

            //allow stop recording
            main.ButtonStop.Visibility = System.Windows.Visibility.Visible;

            //enable scoring buttons
            main.HomeScoringButtons.IsEnabled = true;
            main.GuestScoringButtons.IsEnabled = true;
            main.SetButtons.IsEnabled = true;

            //can't export during recording
            main.ButtonExportVideo.IsEnabled = false;
        }

        private void DispatchTimer_Tick(object sender, EventArgs e)
        {
            main.RecordingTimer = matchRecorder.StopWatch.ToLongElapsedTimeString();
            main.OnPropertyChanged("RecordingTimer");
        }

        internal void StopMatch()
        {
            State = AppStates.Stopped;

            //start match recorder and timers
            matchRecorder.EndMatch();

            //disable scoring buttons
            main.ButtonRecord.IsEnabled = true;
            main.ButtonStop.Visibility = System.Windows.Visibility.Hidden;
            main.HomeScoringButtons.IsEnabled = false;
            main.GuestScoringButtons.IsEnabled = false;
            main.SetButtons.IsEnabled = false;

            //enable actions
            main.ButtonExportVideo.IsEnabled = true;
        }

        private void UpdateScore()
        {
            main.HomeTeamScore = matchRecorder.State.HomeTeamScore;
            main.OnPropertyChanged("HomeTeamScore");
            main.GuestTeamScore = matchRecorder.State.GuestTeamScore;
            main.OnPropertyChanged("GuestTeamScore");
            main.SetNumber = matchRecorder.State.SetNumber;
            main.OnPropertyChanged("SetNumber");
        }

        private void UpdateEventList()
        {
            main.MatchEventCollection.Add(new ViewModel.MatchEventViewModel(
                matchRecorder.GetLastEvent(),
                matchRecorder.State.HomeTeamScore,
                matchRecorder.State.GuestTeamScore,
                matchRecorder.State.SetNumber));
            main.lbLog.SelectedIndex = main.lbLog.Items.Count - 1;
            main.lbLog.ScrollIntoView(main.lbLog.SelectedItem);
            main.OnPropertyChanged("MatchEventCollection");
        }


        internal void HomePointInc()
        {
            if (State != AppStates.Recording)
                return;

            matchRecorder.IncreaseScore(TeamType.Home);
            UpdateEventList();
            UpdateScore();
        }

        internal void HomePointDec()
        {
            if (State != AppStates.Recording)
                return;

            if (matchRecorder.State.HomeTeamScore > 0)
            {
                matchRecorder.DecreaseScore(TeamType.Home);
                UpdateEventList();
                UpdateScore();
            }
        }

        internal void GuestPointInc()
        {
            if (State != AppStates.Recording)
                return;

            matchRecorder.IncreaseScore(TeamType.Guest);
            UpdateEventList();
            UpdateScore();
        }

        internal void GuestPointDec()
        {
            if (State != AppStates.Recording)
                return;

            if (matchRecorder.State.GuestTeamScore > 0)
            {
                matchRecorder.DecreaseScore(TeamType.Guest);
                UpdateEventList();
                UpdateScore();
            }
        }

        internal void SetInc()
        {
            if (State != AppStates.Recording)
                return;

            matchRecorder.IncreaseSet();
            matchRecorder.ResetScore();
            UpdateEventList();
            UpdateScore();
        }

        internal void SetDec()
        {
            if (State != AppStates.Recording)
                return;

            matchRecorder.DecreaseSet();
            UpdateEventList();
            UpdateScore();
        }
    }
}
