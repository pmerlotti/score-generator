﻿using ScoreRecorder.Core;
using System;
using System.Reflection.Emit;

namespace ScoreRecorder.ViewModel
{
    public class MatchEventViewModel: ViewModelBase
    {
        public MatchEventViewModel(MatchEvent matchEvent, int homeScore, int guestScore, int setNumber)
        {
            MatchEventModel = matchEvent;
            HomeScore = homeScore;
            GuestScore = guestScore;
            SetNumber = setNumber;
        }

        public TimeSpan Elapsed => MatchEventModel.EventElapsed;

        public string TimeCode => MatchEventModel.TimeCode;

        public string Description => MatchEventModel.Description;

        public int SetNumber { get; set; }

        public int HomeScore { get; }

        public int GuestScore { get; }

        public MatchEvent MatchEventModel { get; }
    }
}
