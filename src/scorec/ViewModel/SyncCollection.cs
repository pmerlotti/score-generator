﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace ScoreRecorder
{
    public class SyncCollection<TViewModel, TModel> : ObservableCollection<TViewModel>
    {
        readonly IList<TModel> modelCollection;
        readonly Func<TViewModel, TModel> modelExtractorFunc;

        /// <summary>
        /// Creates a new instance of SyncCollection
        /// </summary>
        /// <param name="modelCollection">The list of Models to sync to</param>
        /// <param name="viewModelCreatorFunc">Creates a new ViewModel instance for the given Model</param>
        /// <param name="modelExtractorFunc">Returns the model which is wrapped by the given ViewModel</param>
        public SyncCollection(
            IList<TModel> modelCollection, 
            Func<TModel, TViewModel> viewModelCreatorFunc, 
            Func<TViewModel, TModel> modelExtractorFunc)
        {
            if (viewModelCreatorFunc == null)
                throw new ArgumentNullException("vmCreatorFunc");
            this.modelCollection = modelCollection ?? throw new ArgumentNullException("modelCollection");
            this.modelExtractorFunc = modelExtractorFunc ?? throw new ArgumentNullException("modelExtractorFunc");

            // create ViewModels for all Model items in the modelCollection
            foreach (var model in modelCollection)
                Add(viewModelCreatorFunc(model));

            CollectionChanged += SyncCollection_CollectionChanged;
        }

        private void SyncCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // update the modelCollection accordingly

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    for (int i = 0; i < e.NewItems.Count; i++)
                        modelCollection.Insert(i + e.NewStartingIndex, modelExtractorFunc((TViewModel)e.NewItems[i]));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    // NOTE: currently this ignores the index (works when there are no duplicates in the list)
                    foreach (var vm in e.OldItems.OfType<TViewModel>())
                        modelCollection.Remove(modelExtractorFunc(vm));
                    break;
                case NotifyCollectionChangedAction.Replace:
                    throw new NotImplementedException();
                case NotifyCollectionChangedAction.Move:
                    throw new NotImplementedException();
                case NotifyCollectionChangedAction.Reset:
                    modelCollection.Clear();
                    foreach (var viewModel in this)
                        modelCollection.Add(modelExtractorFunc(viewModel));
                    break;
            }
        }
    }
}
