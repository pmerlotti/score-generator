﻿using Microsoft.Win32;
using ScoreRecorder;
using ScoreRecorder.Controls.Scoreboards;
using ScoreRecorder.Core;
using ScoreRecorder.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace scorec
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private const string WindowTitle = "Score Recorder";

        private MatchRecorder matchRecorder;
        private MatchStopWatch matchStopWatch = new MatchStopWatch();
        private Teams teams = new Teams();
        private ApplicationState appState;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            appState = new ApplicationState(this);

            NewMatchCommand.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            StartStopMatchCommand.InputGestures.Add(new KeyGesture(Key.F5));
            HomePointIncCommand.InputGestures.Add(new KeyGesture(Key.NumPad1));
            HomePointDecCommand.InputGestures.Add(new KeyGesture(Key.NumPad4));
            GuestPointIncCommand.InputGestures.Add(new KeyGesture(Key.NumPad3));
            GuestPointDecCommand.InputGestures.Add(new KeyGesture(Key.NumPad6));
            SetIncCommand.InputGestures.Add(new KeyGesture(Key.NumPad7));
            SetDecCommand.InputGestures.Add(new KeyGesture(Key.NumPad9));
        }

        public static RoutedCommand NewMatchCommand = new RoutedCommand();
        public static RoutedCommand StartStopMatchCommand = new RoutedCommand();
        public static RoutedCommand HomePointIncCommand = new RoutedCommand();
        public static RoutedCommand HomePointDecCommand = new RoutedCommand();
        public static RoutedCommand GuestPointIncCommand = new RoutedCommand();
        public static RoutedCommand GuestPointDecCommand = new RoutedCommand();
        public static RoutedCommand SetIncCommand = new RoutedCommand();
        public static RoutedCommand SetDecCommand = new RoutedCommand();

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<MatchEventViewModel> MatchEventCollection { get; private set; }

        public int SetNumber { get; set; }

        public string HomeTeamName 
        { 
            get => teams.HomeTeamName;
            internal set
            {
                teams.HomeTeamName = value;
                OnPropertyChanged();
            }
        }

        public string HomeTeamShort
        {
            get => teams.HomeTeamShort;
            internal set
            {
                teams.HomeTeamShort = value;
                OnPropertyChanged();
            }
        }

        public int HomeTeamScore { get; set; }

        public string GuestTeamName 
        { 
            get => teams.GuestTeamName;
            internal set
            {
                teams.GuestTeamName = value;
                OnPropertyChanged();
            }
        }

        public string GuestTeamShort
        {
            get => teams.GuestTeamShort;
            internal set
            {
                teams.GuestTeamShort = value;
                OnPropertyChanged();
            }
        }

        public int GuestTeamScore { get; set; }

        public string RecordingTimer { get; internal set; }

        internal void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /* NEW MATCH */

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            NewMatch();
        }

        private void NewMatchCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            NewMatch();
        }

        private void NewMatch_click(object sender, RoutedEventArgs e)
        {
            NewMatch();
        }

        private void NewMatch()
        {
            Title = WindowTitle + " - untitled.match";

            //create new match recorder
            matchRecorder = new MatchRecorder(matchStopWatch, teams);
            MatchEventCollection = new ObservableCollection<MatchEventViewModel>();
            OnPropertyChanged("MatchEventCollection");

            appState.SetInitialState(matchRecorder);
        }

        /* START [RECORDING] AND STOP RECORDING MATCH */

        private void StartStopMatchCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (appState.State != AppStates.Recording)
                appState.StartMatch();
            else
                appState.StopMatch();
        }

        private void StartMatch_click(object sender, RoutedEventArgs e)
        {
            appState.StartMatch(); 
        }

        private void StopMatch_click(object sender, RoutedEventArgs e)
        {
            appState.StopMatch();
        }

        /* SCORING */

        private void HomePointIncCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            appState.HomePointInc();
        }

        private void HomeIncScore_click(object sender, RoutedEventArgs e)
        {
            appState.HomePointInc();
        }

        private void HomePointDecCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            appState.HomePointDec();
        }

        private void HomeDecScore_click(object sender, RoutedEventArgs e)
        {
            appState.HomePointDec();
        }

        private void GuestPointIncCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            appState.GuestPointInc();
        }

        private void GuestIncScore_click(object sender, RoutedEventArgs e)
        {
            appState.GuestPointInc();
        }

        private void GuestPointDecCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            appState.GuestPointDec();
        }

        private void GuestDecScore_click(object sender, RoutedEventArgs e)
        {
            appState.GuestPointDec();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Dispatcher.BeginInvoke(new Action(() => textBox.SelectAll()));
        }


        /* SETS */

        private void SetIncCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            appState.SetInc();
        }

        private void SetInc_click(object sender, RoutedEventArgs e)
        {
            appState.SetInc();
        }

        private void SetDecCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            appState.SetDec();
        }

        private void SetDec_click(object sender, RoutedEventArgs e)
        {
            appState.SetDec();
        }

        /* EXPORT VIDEO */
        private void ButtonExportVideo_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new SaveFileDialog();

            dlg.Filter = "Video file (*.avi)|*.avi";
            dlg.FileName = $"scoreboard_{DateTime.Today:yyyyMMdd}_{HomeTeamShort}_{GuestTeamShort}.avi";
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonVideos);
            dlg.DefaultExt = "avi";
            dlg.Title = "Export video";
            if (dlg.ShowDialog().GetValueOrDefault())
            {
                //write to video
                IScoreboard scoreboard = new SimpleScoreboard();
                var writter = new MatchVideoWritter(matchRecorder.EventLogger, scoreboard, teams);
                writter.CreateVideo(dlg.FileName);
            }

        }


    }
}
