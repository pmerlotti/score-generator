﻿using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ScoreRecorder.Controls.Scoreboards
{
    public partial class SimpleScoreboard : UserControl, IScoreboard, INotifyPropertyChanged
    {
        public SimpleScoreboard()
        {
            InitializeComponent();
            DataContext = this;
        }

        public int SetNumber
        {
            get { return (int)GetValue(SetNumberProperty); }
            set
            {
                SetValue(SetNumberProperty, value);
                OnPropertyChanged();
            }
        }

        public static readonly DependencyProperty SetNumberProperty =
            DependencyProperty.Register("SetNumber", typeof(int),
            typeof(SimpleScoreboard), new PropertyMetadata(0));


        public int HomeScore
        {
            get { return (int)GetValue(HomeScoreProperty); }
            set 
            { 
                SetValue(HomeScoreProperty, value);
                OnPropertyChanged();
            }
        }

        public static readonly DependencyProperty HomeScoreProperty =
            DependencyProperty.Register("HomeScore", typeof(int),
            typeof(SimpleScoreboard), new PropertyMetadata(0));

        public int GuestScore
        {
            get { return (int)GetValue(GuestScoreProperty); }
            set 
            { 
                SetValue(GuestScoreProperty, value);
                OnPropertyChanged();
            }
        }

        public static readonly DependencyProperty GuestScoreProperty =
            DependencyProperty.Register("GuestScore", typeof(int),
            typeof(SimpleScoreboard), new PropertyMetadata(0));

        public string HomeTeam
        {
            get { return (string)GetValue(HomeTeamProperty); }
            set
            {
                SetValue(HomeTeamProperty, value);
                OnPropertyChanged();
            }
        }

        public static readonly DependencyProperty HomeTeamProperty =
            DependencyProperty.Register("HomeTeam", typeof(string),
            typeof(SimpleScoreboard), new PropertyMetadata(""));

        public string GuestTeam
        {
            get { return (string)GetValue(GuestTeamProperty); }
            set
            {
                SetValue(GuestTeamProperty, value);
                OnPropertyChanged();
            }
        }

        public static readonly DependencyProperty GuestTeamProperty =
            DependencyProperty.Register("GuestTeam", typeof(string),
            typeof(SimpleScoreboard), new PropertyMetadata(""));


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public Size ControlSize { get { return new Size(Width, Height); } }

        public System.Drawing.Bitmap Render(IMatchState matchState)
        {
            //update properties
            SetNumber = matchState.SetNumber;
            HomeTeam = matchState.HomeTeamShort;
            HomeScore = matchState.HomeTeamScore;
            GuestTeam = matchState.GuestTeamShort;
            GuestScore = matchState.GuestTeamScore;

            //refresh control, so we can render even with a windowless application
            this.Measure(ControlSize);
            this.Arrange(new Rect(ControlSize));
            UpdateLayout();

            //render control to a bitmap
            var bmp = new RenderTargetBitmap((int)ControlSize.Width, (int)ControlSize.Height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(this);


            var encoder = new PngBitmapEncoder();
            var stream = new MemoryStream();
            encoder.Frames.Add(BitmapFrame.Create(bmp));

            encoder.Save(stream);

            //using (FileStream fileStream = new FileStream("test.png", FileMode.Create))
            //{
            //    encoder.Save(fileStream);
            //}

            
            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(stream);

            return bitmap;
        }

    }
}
