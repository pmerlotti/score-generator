﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ScoreRecorder.Controls.Scoreboards
{
    public interface IScoreboard
    {
        Size ControlSize { get; }

        System.Drawing.Bitmap Render(IMatchState matchState);
        
    }
}
