﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreRecorder.Controls.Scoreboards
{
    public interface IMatchState
    {
        TimeSpan Elapsed { get; }
        int SetNumber { get; set; }
        string HomeTeamShort { get; set; }
        int HomeTeamGamesWon { get; set; }
        int HomeTeamScore { get; set; }
        string GuestTeamShort { get; set; }
        int GuestTeamGamesWon { get; set; }
        int GuestTeamScore { get; set; }

        void SetElapsed(TimeSpan elapsed);
    }
}
